.. _header-n0:

反馈/建议及其他
===============

-  线上提交反馈

   -  http://tp.songqingliang.cn

-  线下TP会反馈建议

   -  每周四下午16:00-17:00

-  联系作者

   -  SQ，产品经理

   -  pm@songqingliang.cn

-  进行中的工作

   -  3.9的需求

-  其他说明

   -  产品其他说明 https://docs.qq.com/doc/DWmdWSGR4WHpaQ0ZF

-  产品开发团队

   -  XQ、KXB、HS、XKJ、SXJ、ZCL、ZTT…...

   -  [STRIKEOUT:WCH]

.. _header-n0:

小程序交易平台入门指南
======================

.. contents::

.. _header-n3:

1.访问和注册
------------

-  访问

   -  访问方式1 在微信中搜索 “博达看板” 关键词查找到小程序访问

   -  访问方式2
      微信“扫一扫”如下图片或微信中长按此图片“识别图中小程序码”访问\ |image0|

-  注册

   -  进入注册页面\ |image1|

   -  填写信息进行注册

.. _header-n7:

2.登录
------

-  进入登录页\ |image2|

   -  登录方式1

      -  微信一键授权手机号登录，选择 允许
         进行登录（如授权的手机号未注册看板帐号，请跳转到登录页面后选择注册进行注册帐号）\ |image3|

   -  登录方式2

      -  选择账号密码登录，进入登录页面\ |image4|

      -  填写账号密码进行登录

.. _header-n11:

3.下单
------

-  进入首页\ |image5|

-  选择城市（可通过允许获取位置快速选择位置所在的城市）、选择排期日期范围后，点击“查找媒体资源”，进入点位/媒体列表（登录状态下)\ |image6|

-  可点击右侧的 地图模式 图标，进入点位的地图视图\ |image7|

-  点击某一点位进入点位详情，并选择需要添加的 媒体（点击加号➕添加）

-  选择添加好媒体，点击购物车图标进入购物车去下单，并提交订单。提交后选择支付方式进行支付（建议选择微信支付进行支付）\ |image8|

.. _header-n19:

4.方案
------

小程序端暂时无法创建创建，创建方案请使用电脑访问https://www.topsboard.com

-  公开状态的方案详情页，在小程序中点击右上角的“转发”可以转发给微信的好友

.. |image0| image:: http://w.okaysong.cn/Jietu20190308-151327@2x.jpg
.. |image1| image:: http://w.okaysong.cn/WechatIMG141.png
.. |image2| image:: http://w.okaysong.cn/WechatIMG143.png
.. |image3| image:: http://w.okaysong.cn/WechatIMG144.png
.. |image4| image:: http://w.okaysong.cn/WechatIMG140.png
.. |image5| image:: http://w.okaysong.cn/WechatIMG142.png
.. |image6| image:: http://w.okaysong.cn/WechatIMG145.png
.. |image7| image:: http://w.okaysong.cn/WechatIMG146.png
.. |image8| image:: http://w.okaysong.cn/WechatIMG148.png

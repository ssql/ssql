.. _header-n0:

web交易平台入门指南
===================

.. contents::

.. _header-n5:

1.访问和注册
------------

-  访问注册地址https://www.topsboard.com/#/main/register |image0|

-  填写信息并获取手机验证码填写后，点击 “即刻开始” 提交注册

.. _header-n12:

2.登录
------

-  访问登录地址https://www.topsboard.com/#/main/login |image1|

.. _header-n17:

3.认证
------

-  在个人中心中，点击 “认证信息”进入认证信息页面。如未注册点击 “未认证”
   进入提交申请认证页面（提交申请认证地址https://www.topsboard.com/#/main/auth
   |image2|

-  进入提交申请认证，填写信息后，点击“提交”，提交申请认证信息\ |image3|

.. _header-n24:

4.订单
------

-  访问交易平台网址https://www.topsboard.com/#/home |image4|

-  选择城市、选择排期日期范围后，点击“制定计划”，进入投放制定页面（登录状态下）

-  进入投放制定页面，可查看到所选城市、所选排期范围下的可用点位/媒体列表和地图视图\ |image5|

-  （可选操作）如需框选特定范围下的点位/媒体，可点击右上角的框选图标进行框选范围

-  （可选操作）如需筛选某一区域、某一媒体类型等，可点击 “类型筛选“
   进行筛选\ |image6|

-  选中某一点位，进入媒体列表。可选择点位下的某一媒体，点击
   ”加号➕“将媒体加入购物车\ |image7|

-  完成选择后媒体后，点击 ”查看购物车“
   进入购物车页面（可选操作：可根据需要移除某一媒体；也可选择媒体另存为投放方案）。在购物车页面下，审视确认后，点击”去下单“
   进入确认订单页面\ |image8|

-  在确认订单页，选择进行支付的方式（支持在线的微信支付、支付宝；线下对公转账）\ |image9|

-  如需发票，可选择修改完善发票信息。审视订单后，点击“提交并支付”提交生成订单并进行支付（支付宝支付方式，将跳转到第三方支付宝的支付页面；微信支付方式，将跳转到微信支付页面；对公转账方式，将生成订单并进入订单列表页面，由线下进行手动转账付款（须附言备注订单编号））\ |image10|

-  如选择微信支付，提交订单后，将进入微信支付页面，使用手机微信“扫一扫”进行扫码付款\ |image11|

-  进入我的订单页面，查看已提交生成的订单（未付款的订单须在一定时间内进行付款，否则将自动取消关闭订单；付款完成后，须媒体主操作确认订单或者撤销关闭订单）\ |image12|

-  点击某一订单的“详情”，进入订单详情页面（如是需要分多次进行付款的订单，须在付款开启后进行付款；订单在已上刊状态前申请退款；媒体主在操作上刊后，可在订单详情中操作确认验收）\ |image13|

.. _header-n50:

5.方案
------

-  由购物车另存为保存的方案，可在个人中心 “我的方案”
   中查看，可分享方案、编辑方案、删除方案、方案下单\ |image14|

.. |image0| image:: http://w.okaysong.cn/Jietu20181122-160458@2x.jpg
.. |image1| image:: http://w.okaysong.cn/Jietu20181122-160508@2x.jpg
.. |image2| image:: http://w.okaysong.cn/Jietu20181122-160051@2x.jpg
.. |image3| image:: http://w.okaysong.cn/Jietu20181122-160445@2x.jpg
.. |image4| image:: http://w.okaysong.cn/Jietu20181122-155630@2x.jpg
.. |image5| image:: http://w.okaysong.cn/Jietu20181122-155651@2x.jpg
.. |image6| image:: http://w.okaysong.cn/Jietu20181122-163703@2x.jpg
.. |image7| image:: http://w.okaysong.cn/Jietu20181122-155708@2x.jpg
.. |image8| image:: http://w.okaysong.cn/Jietu20181122-155751@2x.jpg
.. |image9| image:: http://w.okaysong.cn/Jietu20181122-155823@2x.jpg
.. |image10| image:: http://w.okaysong.cn/Jietu20181122-155840@2x.jpg
.. |image11| image:: http://w.okaysong.cn/Jietu20181122-155914@2x.jpg
.. |image12| image:: http://w.okaysong.cn/Jietu20181122-155939@2x.jpg
.. |image13| image:: http://w.okaysong.cn/Jietu20181122-160010@2x.jpg
.. |image14| image:: http://w.okaysong.cn/Jietu20181122-160034@2x.jpg

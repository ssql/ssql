.. _header-n0:

SaaS入门指南
============

.. contents::

.. _header-n3:

0.组织/人员、角色/权限
----------------------

-  按实际业务情况分配角色/权限，添加组织/人员并赋予角色权限

   .. figure:: http://w.okaysong.cn/Jietu20181212-152932.jpg
      :alt: 

.. figure:: /Users/c/jianguoyun/carver/creative/Jietu20181212-152932.jpg
   :alt: 

.. _header-n9:

1、点位/媒体管理
----------------

.. _header-n10:

1.1 点位管理
~~~~~~~~~~~~

添加点位资源 点位资源中点击“添加点位”

.. figure:: http://w.okaysong.cn/image-20180515093452651.png
   :alt: 

-  **方式1:批量导入点位**

   -  第一步：点击“下载模版”，并按列表项填充点位数据\ |image0|

   -  第二步：点击“上传模版”，选择已填充好点位数据的表格文件并上传\ |image1|

   -  第三步：点击“保存”\ |image2|

-  **方式2:单个添加点位**

   -  第一步：点击“单个添加”\ |image3|

   -  第二步：填写完善点位数据\ |image4|

   -  第三步：点击“保存”\ |image5|

.. _header-n32:

1.2 媒体管理
~~~~~~~~~~~~

添加媒体资源 媒体资源中点击“添加媒体”\ |image6|

-  **方式一：批量导入媒体**

   -  第一步：点击“下载模版”，并按列表项填充媒体数据\ |image7|

   -  第二步：点击“上传模版”，选择已填充好媒体数据的表格文件并上传\ |image8|

   -  第三步：点击“保存”\ |image9|

-  **方式二：点个添加媒体**

   -  第一步：点击“单个添加”\ |image10|

   -  第二步：选择媒体所在的点位并填写完善媒体数据\ |image11|

   -  第三步：点击“保存”\ |image12|

.. _header-n53:

2、客户/项目管理
----------------

.. _header-n54:

2.1 客户管理
~~~~~~~~~~~~

-  第一步：项目管理-客户管理中 点击“添加客户”\ |image13|

-  第二步：填写完善客户信息\ |image14|

-  第三步：点击“确定”保存\ |image15|

.. _header-n62:

2.2 项目管理
~~~~~~~~~~~~

-  第一步：项目管理中 点击“添加项目”

-  第二步：选择项目所属的客户并填写完善项目信息

-  第三步：点击“确定”保存

.. _header-n70:

3、方案管理
-----------

.. _header-n71:

3.1 通过地图查看媒体资源/导出资源
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-  第一步：点击 媒体资源中的“地图筛选模式 ”图标\ |image16|

-  第二步（可选操作）：点击“类型筛选”按需要进行媒体的筛选，筛选后点击“确定”后查看筛选后的媒体列表/地图点位\ |image17|\ |image18|

-  第三步（可选操作）：点击“圈选地图”按需要区域进行圈选地图筛选媒体（提供圆形圈选、方形圈选、不规则图形、中心位置半径公里圈选），圈选后点击“确认”后查看筛选后的媒体列表/地图点位\ |image19|\ |image20|

-  第四步：点击“导出列表”，导出当前结果的媒体列表文件\ |image21|

.. _header-n81:

3.2 创建方案/下单 
~~~~~~~~~~~~~~~~~~

-  第一步：点击方案列表中的 “创建方案”\ |image22|

-  第二步：选择方案所属的项目并填写完善方案信息，点击“下一步”进入下一步\ |image23|

-  第三步：点击投放排期，选择需要投放的日期范围\ |image24|\ |image25|

-  第四步（可选操作）：筛选资源。根据需要通过筛选查找需要的媒体资源，筛选后点击“筛选”查看筛选后的媒体资源\ |image26|

-  第五步（可选操作）：圈选地图资源。根据需要圈选地图查找需要的媒体资源，圈选地图后，点击“确认”查看查看筛选后的媒体资源\ |image27|

-  第六步：添加媒体资源到购物车\ |image28|

   -  **添加媒体资源到购物车方式一：单个媒体资源添加到购物车**\ 。点击对应的点位，然后点击媒体的家加号➕，将单个媒体添加到购物车\ |image29|\ |image30|

   -  **添加媒体资源到购物车方式二：批量添加点位下的媒体资源到购物车**\ 。勾选☑️需要的点位后，点击“加入购物车”，将勾选☑️下的可用媒体添加到购物车\ |image31|

   -  **添加媒体资源到购物车方式二：智能调点**\ 。按媒体类型筛选（电梯、道闸、LED）后，根据需要点击“智能调点”后，选择需要添加到购物车的媒体资源条件，点击“加入购物车”后，符合条件的可用媒体将添加到购物车\ |image32|\ |image33|

-  第七步：选择好媒体资源后，点击“下单”后，填写成交价格后，点击“确认”下单并占用对应媒体资源的排期。如暂不下单，可点击“保存“当前的投放方案（保存的方案中的媒体资源不占用媒体资源的排期），后续需要编辑/下单可在方案列表中再编辑/下单（已保存的方案可在方案详情中，点击“导出”导出方案的媒体列表或点位列表）\ |image34|

.. _header-n105:

4、订单管理
-----------

-  待确认的订单：根据业务需要，点击“确认订单”完成确认（确认后的订单无法再撤销）；如点击“撤销订单”将关闭订单并释放该订单下的媒体资源的排期\ |image35|

-  在订单中的媒体列表可导出媒体列表、点位列表、制作列表、完工报告\ |image36|\ |image37|

-  在订单-验收报告中，可上传点位的上刊图片后，点击“导出验收报告”导出验收报告ppt\ |image38|

.. |image0| image:: http://w.okaysong.cn/image-20180515093627349.png
.. |image1| image:: http://w.okaysong.cn/image-20180515094021406.png
.. |image2| image:: http://w.okaysong.cn/image-20180515094036243.png
.. |image3| image:: http://w.okaysong.cn/image-20180515094159902.png
.. |image4| image:: http://w.okaysong.cn/image-20180515094422178.png
.. |image5| image:: http://w.okaysong.cn/image-20180515094559549.png
.. |image6| image:: http://w.okaysong.cn/image-20180515094820408.png
.. |image7| image:: http://w.okaysong.cn/image-20180515094958195.png
.. |image8| image:: http://w.okaysong.cn/image-20180515095113585.png
.. |image9| image:: http://w.okaysong.cn/image-20180515095134701.png
.. |image10| image:: http://w.okaysong.cn/image-20180515095320212.png
.. |image11| image:: http://w.okaysong.cn/image-20180515095618842.png
.. |image12| image:: http://w.okaysong.cn/image-20180515095700708.png
.. |image13| image:: http://w.okaysong.cn/image-20180515095907150.png
.. |image14| image:: http://w.okaysong.cn/image-20180515100008151.png
.. |image15| image:: http://w.okaysong.cn/image-20180515105455364.png
.. |image16| image:: http://w.okaysong.cn/image-20180515105631984.png
.. |image17| image:: http://w.okaysong.cn/image-20180515105905152.png
.. |image18| image:: http://w.okaysong.cn/image-20180515110023128.png
.. |image19| image:: http://w.okaysong.cn/image-20180515105936298.png
.. |image20| image:: http://w.okaysong.cn/image-20180515112455271.png
.. |image21| image:: http://w.okaysong.cn/image-20180515112711447.png
.. |image22| image:: http://w.okaysong.cn/image-20180515112953611.png
.. |image23| image:: http://w.okaysong.cn/image-20180515113023911.png
.. |image24| image:: http://w.okaysong.cn/image-20180515113508069.png
.. |image25| image:: http://w.okaysong.cn/image-20180515113554811.png
.. |image26| image:: http://w.okaysong.cn/image-20180515113203510.png
.. |image27| image:: http://w.okaysong.cn/image-20180515113438088.png
.. |image28| image:: 
.. |image29| image:: http://w.okaysong.cn/image-20180515113651133.png
.. |image30| image:: http://w.okaysong.cn/image-20180515113731678.png
.. |image31| image:: http://w.okaysong.cn/image-20180515113333806.png
.. |image32| image:: http://w.okaysong.cn/image-20180515125608304.png
.. |image33| image:: http://w.okaysong.cn/image-20180515125643790.png
.. |image34| image:: http://w.okaysong.cn/image-20180515130324263.png
.. |image35| image:: http://w.okaysong.cn/image-20180515130741176.png
.. |image36| image:: http://w.okaysong.cn/image-20180515130848596.png
.. |image37| image:: http://w.okaysong.cn/image-20180515130926575.png
.. |image38| image:: http://w.okaysong.cn/image-20180515131008556.png
